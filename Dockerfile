FROM openjdk:8

COPY devops_sample/target/assignment*.jar .

EXPOSE 8090

ENTRYPOINT  java -jar -Dspring.profiles.active=h2 assignment*.jar &

